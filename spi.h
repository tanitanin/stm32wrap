/*
 * spi.h
 *
 *  Created on: 2012/12/08
 *      Author: tanitanin
 */

#ifndef SPI_H_
#define SPI_H_


#include "core.h"


template <const Address SPIx>
class SPI {
public:
  static void init(SPI_InitTypeDef &st) {
    if(SPIx==SPI1_BASE) {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1,ENABLE);
    } else if(SPIx==SPI2_BASE) {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2,ENABLE);
    } else if(SPIx==SPI3_BASE) {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI3,ENABLE);
    }

    SPI_Init((SPI_TypeDef *)SPIx, &st);
  }

  static void enable() {
    SPI_Cmd((SPI_TypeDef *)SPIx, ENABLE);
  }

  static void disable() {
    SPI_Cmd((SPI_TypeDef *)SPIx, DISABLE);
  }

};


typedef SPI<SPI1_BASE> spi1;
typedef SPI<SPI2_BASE> spi2;
typedef SPI<SPI3_BASE> spi3;


#endif /* SPI_H_ */
