/*
 * usart.h
 *
 *  Created on: 2012/12/07
 *      Author: tanitanin
 */

#ifndef USART_H_
#define USART_H_

#include <stdlib.h>
#include <string.h>
#include "core.h"

template <const Address USARTx>
class USART {
public:
  static void init(USART_InitTypeDef &st) {
    if(USARTx==USART1_BASE) {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);
    } else if(USARTx==USART2_BASE) {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);
    } else if(USARTx==USART3_BASE) {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3,ENABLE);
    } else if(USARTx==UART4_BASE) {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4,ENABLE);
    } else if(USARTx==UART5_BASE) {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5,ENABLE);
    }

    USART_Init((USART_TypeDef *)USARTx, &st);
  }

  static void enable() {
    USART_Cmd((USART_TypeDef *)USARTx, ENABLE);
  }

  static void disable() {
    USART_Cmd((USART_TypeDef *)USARTx, DISABLE);
  }

  static void put_char(char data) {
    USART_SendData((USART_TypeDef *)USARTx, data);
    while(USART_GetFlagStatus((USART_TypeDef *)USARTx, USART_FLAG_TXE) == 0);
  }

  static void put_str(char *str) {
    char *ptr = str;
    while(*ptr!='\0') put_char(*ptr++);
  }

  static void put_decimal(const int num) {
    char buf[12];
    itoa(num, buf);
    put_str(buf);
  }

  static unsigned short get_char() {
    while(USART_GetFlagStatus((USART_TypeDef *)USARTx, USART_FLAG_RXNE) == 1);
    return USART_ReceiveData((USART_TypeDef *)USARTx);
  }

  static char *get_str() {
    char buf[512]; int i=0;
    while((buf[i++]=get_char())!='\0' and i<512);
    return buf;
  }


private:
  static void itoa(int n, char *s) {
    int i, sign;
    if ((sign = n) < 0)  /* record sign */
      n = -n;          /* make n positive */
    i = 0;
    do {       /* generate digits in reverse order */
        s[i++] = n % 10 + '0';   /* get next digit */
    } while ((n /= 10) > 0);     /* delete it */
    if (sign < 0)
      s[i++] = '-';
    s[i] = '\0';
    reverse(s);
  }

  static void reverse(char *s) {
    char *j;
    char c;
    j = s; while(*j!='\0') j++; j--;
    while(s < j) {
      c = *s;
      *s++ = *j;
      *j-- = c;
    }
  }


};


typedef USART<USART1_BASE> usart1;
typedef USART<USART2_BASE> usart2;
typedef USART<USART3_BASE> usart3;
//typedef USART<UART4_BASE> uart4;
//typedef USART<UART5_BASE> uart5;

#endif /* USART_H_ */
