/*
 * gpio.h
 *
 *  Created on: 2012/12/07
 *      Author: tanitanin
 */

#ifndef GPIO_H_
#define GPIO_H_

#include "core.h"

template <const Address GPIOx>
class GPIO {
public:
  static void init(GPIO_InitTypeDef &st) {
    if(GPIOx==GPIOA_BASE) RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
    else if(GPIOx==GPIOB_BASE) RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
    else if(GPIOx==GPIOC_BASE) RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
    else if(GPIOx==GPIOD_BASE) RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOD, ENABLE);
    else if(GPIOx==GPIOE_BASE) RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOE, ENABLE);
    else if(GPIOx==GPIOF_BASE) RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOF, ENABLE);

    GPIO_Init((GPIO_TypeDef *)GPIOx, &st);
  }

  static void write(unsigned short data) {
    GPIO_Write((GPIO_TypeDef *)GPIOx, data);
  }
  static void writeBit(unsigned short pin, unsigned char data) {
    GPIO_WriteBit((GPIO_TypeDef *)GPIOx, 1<<pin, (data!=0?Bit_SET:Bit_RESET));
  }

  static unsigned short readInput() {
    return GPIO_ReadInputData((GPIO_TypeDef *)GPIOx);
  }
  static unsigned char readInputBit(unsigned short pin) {
    return GPIO_ReadInputDataBit((GPIO_TypeDef *)GPIOx, 1<<pin);
  }

  static unsigned short readOutput() {
    return GPIO_ReadInputData((GPIO_TypeDef *)GPIOx);
  }
  static unsigned char readOutputBit(unsigned short pin) {
    return GPIO_ReadInputDataBit((GPIO_TypeDef *)GPIOx, 1<<pin);
  }

  static void lock(unsigned short pin) {
    GPIO_PinLockConfig((GPIO_TypeDef *)GPIOx, 1<<pin);
  }

  static void alternate(unsigned short pin, unsigned char GPIO_AF) {
    GPIO_PinAFConfig((GPIO_TypeDef *)GPIOx, pin, GPIO_AF);
  }

};

typedef GPIO<GPIOA_BASE> gpioa;
typedef GPIO<GPIOB_BASE> gpiob;
typedef GPIO<GPIOC_BASE> gpioc;
typedef GPIO<GPIOD_BASE> gpiod;
typedef GPIO<GPIOE_BASE> gpioe;
typedef GPIO<GPIOF_BASE> gpiof;


#endif /* GPIO_H_ */
