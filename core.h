/*
 * core.h
 *
 *  Created on: 2012/12/07
 *      Author: tanitanin
 */

#ifndef CORE_H_
#define CORE_H_


#ifdef STM32F10X
 #include <stm32f10x_rcc.h>
 #include <stm32f10x_gpio.h>
 #include <stm32f10x_usart.h>
#endif
#ifdef STM32F30X
 #include <stm32f30x_rcc.h>
 #include <stm32f30x_gpio.h>
 #include <stm32f30x_usart.h>
#endif
#ifdef STM32F4XX
 #include <stm32f4xx_rcc.h>
 #include <stm32f4xx_gpio.h>
 #include <stm32f4xx_usart.h>
#endif


typedef unsigned long Address;


#endif /* CORE_H_ */
